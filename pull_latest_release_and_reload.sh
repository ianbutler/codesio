source ./.env.snippy
docker login -u gitlab-ci-token -p $GITLAB_REGISTRY_TOKEN registry.gitlab.com
docker-compose pull
docker-compose up -d
