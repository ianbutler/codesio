const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
module.exports = {
    entry: { app: './js/app.js', vendor: ['./vendor/tagify.js'] },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.(css|scss)$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({filename: '../css/app.css'}),
        new CopyWebpackPlugin([{ from: 'static/', to: '../' }])
    ],
    output: {
        path: path.resolve(__dirname, '../priv/static/js'),
        filename: '[name].js',
        publicPath: '/',
    },
    // optimization: {
    //     splitChunks: {
    //         cacheGroups: {
    //             vendor: {
    //                 chunks: 'initial',
    //                 name: 'vendor',
    //                 test: 'vendor',
    //                 enforce: true
    //             },
    //             app: {
    //                 chunks: "initial",
    //                 name: "app",
    //                 test: "app",
    //                 enforce: true
    //             }
    //         }
    //     },
    //     runtimeChunk: true
    // }
};
